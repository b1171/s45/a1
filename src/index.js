import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/*const name = "John Smith";
const element = <h1>Hello, {name}</h1>;

ReactDOM.render(
  element, document.getElementById('root')
)*/

/*const fullName = {
  firstName: "John",
  lastName: "Smith"
}

function getFullName() {
  return `${fullName.firstName} ${fullName.lastName}`;
}

const element = <h1>Full name: {getFullName()}</h1>

ReactDOM.render(
  element,
  document.getElementById('root')
)
*/

