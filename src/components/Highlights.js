
import {Container, Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	
	return (

		<Container fluid className="my-3">
			<Row className="justify-content-center">
				<Col xs={10} md={3} className="my-2">					
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>

				<Col xs={10} md={3} className="my-2">					
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Study Now, Pay Later</Card.Title>
					    <Card.Text>
					      	Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>

				<Col xs={10} md={3} className="my-2">
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Be Part of Our Community</Card.Title>
					    <Card.Text>
					     	Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor veniam quibusdam placeat itaque dolorum et alias expedita excepturi facere cum exercitationem vitae voluptatibus aspernatur quas, consequatur natus porro nobis eos?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}

