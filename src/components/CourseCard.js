
import {Container, Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard() {
	
	return (

		<Container fluid className="my-4">
			<Row className="justify-content-center">
				<Col xs={10} md={9}>					
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Sample Course</Card.Title>
					    
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>
					    	This is a sample course offering.
					    </Card.Text>
					    
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>
					    	PhP 40,000
					    </Card.Text>

					    <Button variant="primary">Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>

			</Row>
		</Container>

	)
}

